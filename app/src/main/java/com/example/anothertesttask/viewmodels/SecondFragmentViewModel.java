package com.example.anothertesttask.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.anothertesttask.room.ImageEntity;
import com.example.anothertesttask.room.LocalRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class SecondFragmentViewModel extends ViewModel {
    private MutableLiveData<List<ImageEntity>> favoritePictureLiveData = new MutableLiveData<>();
    private LocalRepository localRepository;
    private Disposable disposable;

    @Inject
    public SecondFragmentViewModel(LocalRepository localRepository) {
        this.localRepository = localRepository;
    }

    public MutableLiveData<List<ImageEntity>> getFavoritePictureLiveData() {
        if (favoritePictureLiveData.getValue() == null) {
            setLiveData();
        }
        return favoritePictureLiveData;
    }

    public void setLiveData() {
        disposable = localRepository.getFavorite().subscribe(v -> favoritePictureLiveData.postValue(v));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
    }

}