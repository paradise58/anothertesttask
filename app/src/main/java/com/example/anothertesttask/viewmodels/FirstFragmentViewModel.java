package com.example.anothertesttask.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.example.anothertesttask.room.ImageEntity;
import com.example.anothertesttask.room.LocalRepository;
import com.example.anothertesttask.worker.ImageWorker;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class FirstFragmentViewModel extends ViewModel {
    LocalRepository localRepository;
    private MutableLiveData<ImageEntity> pictureUrlLiveData = new MutableLiveData<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public FirstFragmentViewModel(LocalRepository localRepository) {
        this.localRepository = localRepository;
    }


    public MutableLiveData<ImageEntity> getPictureLiveData() {
        startRequest();
        return pictureUrlLiveData;
    }

    private void startRequest() {
        if (pictureUrlLiveData.getValue() == null) {
            compositeDisposable.add(localRepository.getObservableLast().subscribe(v -> pictureUrlLiveData.postValue(v)));
            compositeDisposable.add(Observable.interval(0, 15, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.io())
                    .subscribe(v -> {
                        OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(ImageWorker.class).build();
                        WorkManager.getInstance().enqueue(workRequest);
                    })
            );
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (!compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }

    public void update(ImageEntity image) {
        localRepository.updateImage(image);
    }

    public void insert(ImageEntity image) {
        localRepository.insertImage(image);
    }

    public void delete(String uri) {
        localRepository.deleteImageByUri(uri);
    }
}
