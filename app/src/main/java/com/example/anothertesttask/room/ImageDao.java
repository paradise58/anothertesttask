package com.example.anothertesttask.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Observable;

@Dao
public interface ImageDao {

    @Query("SELECT * FROM imageentity")
    List<ImageEntity> getAll();

    @Query("SELECT * FROM imageentity WHERE isLast =:isLast")
    Observable<ImageEntity> getObservableLast(boolean isLast);

    @Query("SELECT * FROM imageentity WHERE isLast =:isLast")
    ImageEntity getLast(boolean isLast);

    @Query("SELECT * FROM imageentity WHERE isLast =:isLast")
    Observable<List<ImageEntity>> getAllFavorite(boolean isLast);

    @Update
    void update(ImageEntity imageEntity);

    @Insert
    void insert(ImageEntity imageEntity);

    @Query("DELETE  FROM imageentity WHERE uri =:uri AND isLast =:isLast")
    void delete(String uri, boolean isLast);

}
