package com.example.anothertesttask.room;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class LocalRepository {
    ImageDao imageDao;

    @Inject
    public LocalRepository(AppDataBase dataBase) {
        this.imageDao = dataBase.imageDao();
    }

    public ImageEntity getLast() {
        return imageDao.getLast(true);
    }

    public Observable<List<ImageEntity>> getFavorite() {
        return imageDao.getAllFavorite(false);
    }

    public Observable<ImageEntity> getObservableLast() {
        return imageDao.getObservableLast(true);
    }

    public void insertImage(ImageEntity image) {
        Completable.fromAction(() -> imageDao.insert(image)).subscribeOn(Schedulers.io()).subscribe();
    }

    public void updateImage(ImageEntity image) {
        Completable.fromAction(() -> imageDao.update(image)).subscribeOn(Schedulers.io()).subscribe();
    }

    public void deleteImageByUri(String uri) {
        Completable.fromAction(() -> imageDao.delete(uri, false)).subscribeOn(Schedulers.io()).subscribe();
    }
}
