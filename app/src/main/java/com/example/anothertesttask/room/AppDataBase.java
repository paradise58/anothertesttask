package com.example.anothertesttask.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {ImageEntity.class}, version = 2)
public abstract class AppDataBase extends RoomDatabase {
    public abstract ImageDao imageDao();

    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(final SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE ImageEntity ADD COLUMN likeCounts INTEGER DEFAULT 0 NOT NULL");
        }
    };
}
