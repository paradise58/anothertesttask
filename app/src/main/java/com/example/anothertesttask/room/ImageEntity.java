package com.example.anothertesttask.room;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ImageEntity {

    @PrimaryKey(autoGenerate = true)
    public long id;

    public String uri;

    boolean isLast;

    boolean isFavorite;

    int likeCounts;

    public void setLikeCounts(int likeCounts) {
        this.likeCounts = likeCounts;
    }

    public int getLikeCounts() {
        return likeCounts;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public boolean isLast() {
        return isLast;
    }

    public void setLast(boolean last) {
        isLast = last;
    }
}
