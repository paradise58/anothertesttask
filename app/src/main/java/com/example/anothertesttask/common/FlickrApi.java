package com.example.anothertesttask.common;

import com.example.anothertesttask.pojo.MyPicture;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FlickrApi {

    String API_KEY = "6736e7e1cd126ffaa0e0749dcb909d82";

    String methodGetRecentPhotos = "flickr.photos.getRecent";

    @GET("rest/")
    Call<MyPicture> getLastPicture(@Query("method") String method, @Query("api_key") String api_key
            , @Query("per_page") int perPage
            , @Query("format") String format
            , @Query("nojsoncallback") int noJsonCallBack);
}
