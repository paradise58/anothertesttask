package com.example.anothertesttask.common;

import com.example.anothertesttask.pojo.MyPicture;
import com.example.anothertesttask.pojo.Photo;

public class UrlUtil {
    public static String getImageUrl(MyPicture picture) {
        Photo photo = picture.getPhotos().getPhoto().get(0);
        return "https://farm" + photo.getFarm() + ".staticflickr.com/" + photo.getServer() + "/" + photo.getId() + "_" + photo.getSecret() + ".jpg";
    }
}
