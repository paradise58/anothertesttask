package com.example.anothertesttask.common;

import android.app.Application;

import com.example.anothertesttask.dagger.component.AppComponent;
import com.example.anothertesttask.dagger.component.DaggerAppComponent;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.stetho.Stetho;

public class App extends Application {
    private static App instance;
    private AppComponent appComponent;
    public static final String appName = "anotherTestTask";
    public static final String SHARED_IS_APP_LAUNCHED = "isAppLaunched";
    public static final String SHARED_IS_FIRST_START = "isFirstStart";

    public static App getInstance() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Fresco.initialize(this);
        initStetho();

    }

    private void initStetho() {
        Stetho.InitializerBuilder initializerBuilder = Stetho.newInitializerBuilder(this);
        initializerBuilder.enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this));
        initializerBuilder.enableDumpapp(Stetho.defaultDumperPluginsProvider(this));
        Stetho.Initializer initializer = initializerBuilder.build();
        Stetho.initialize(initializer);
    }


    public AppComponent getAppComponent() {
        if (appComponent == null) appComponent = DaggerAppComponent.builder().build();
        return appComponent;
    }
}
