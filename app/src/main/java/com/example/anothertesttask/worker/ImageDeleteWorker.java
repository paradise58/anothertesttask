package com.example.anothertesttask.worker;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.anothertesttask.common.App;
import com.example.anothertesttask.room.LocalRepository;

import javax.inject.Inject;

public class ImageDeleteWorker extends Worker {
    @Inject
    LocalRepository localRepository;

    public ImageDeleteWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        App.getInstance().getAppComponent().inject(this);

        String uri = getInputData().getString("uri");
        localRepository.deleteImageByUri(uri);
        return Result.success();
    }
}
