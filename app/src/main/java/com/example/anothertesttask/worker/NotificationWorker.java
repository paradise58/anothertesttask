package com.example.anothertesttask.worker;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.RingtoneManager;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.anothertesttask.R;
import com.example.anothertesttask.common.App;
import com.example.anothertesttask.common.FlickrApi;
import com.example.anothertesttask.common.UrlUtil;
import com.example.anothertesttask.pojo.MyPicture;
import com.example.anothertesttask.room.LocalRepository;
import com.example.anothertesttask.view.MainActivity;

import java.io.IOException;
import java.util.Objects;

import javax.inject.Inject;

import retrofit2.Response;

import static androidx.core.content.ContextCompat.getSystemService;

public class NotificationWorker extends Worker {
    private final String CHANNEL_ID = "firstChanel";
    private Context context;
    @Inject
    FlickrApi flickrApi;
    @Inject
    LocalRepository localRepository;
    Resources resources;

    public NotificationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
        resources = context.getResources();
    }

    @NonNull
    @Override
    public Result doWork() {
        App.getInstance().getAppComponent().inject(this);

        if (hasNewImage() && !isActivityRunning()) {

            Notification notification =
                    new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_favorite_border_black_24dp)
                            .setContentTitle(resources.getString(R.string.notification_title))
                            .setContentText(resources.getString(R.string.notification_description))
                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                            .setVibrate(new long[]{0, 400, 200, 400})
                            .setContentIntent(createNotificationAction())
                            .build();

            notification.ledARGB = Color.RED;
            notification.ledOffMS = 500;
            notification.ledOnMS = 100;
            notification.flags = notification.flags | Notification.FLAG_SHOW_LIGHTS;

            NotificationManager notificationManager =
                    getSystemService(context, NotificationManager.class);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "My channel",
                        NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription("My channel description");
                channel.enableLights(true);
                channel.setLightColor(Color.RED);
                channel.enableVibration(true);
                Objects.requireNonNull(notificationManager).createNotificationChannel(channel);
            }

            Objects.requireNonNull(notificationManager).notify(1, notification);
        }
        return Result.success();
    }

    private boolean hasNewImage() {
        String lastImageUri = localRepository.getLast().getUri();
        try {
            Response<MyPicture> response = flickrApi.getLastPicture(FlickrApi.methodGetRecentPhotos, FlickrApi.API_KEY
                    , 1, "json", 1).execute();
            if (response.isSuccessful() && response.body() != null) {
                return !lastImageUri.equals(UrlUtil.getImageUrl(response.body()));
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return false;
    }

    private PendingIntent createNotificationAction() {
        Intent intent = new Intent(context, MainActivity.class);
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private Boolean isActivityRunning() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(App.appName, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(App.SHARED_IS_APP_LAUNCHED, false);
    }
}
