package com.example.anothertesttask.worker;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.anothertesttask.common.App;
import com.example.anothertesttask.common.FlickrApi;
import com.example.anothertesttask.common.UrlUtil;
import com.example.anothertesttask.pojo.MyPicture;
import com.example.anothertesttask.room.ImageEntity;
import com.example.anothertesttask.room.LocalRepository;

import java.io.IOException;

import javax.inject.Inject;

import retrofit2.Response;

public class ImageWorker extends Worker {
    @Inject
    FlickrApi flickrApi;
    @Inject
    LocalRepository localRepository;

    public ImageWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }


    @NonNull
    @Override
    public Result doWork() {
        App.getInstance().getAppComponent().inject(this);
        try {
            Response<MyPicture> response = flickrApi.getLastPicture(FlickrApi.methodGetRecentPhotos
                    , FlickrApi.API_KEY, 1, "json", 1).execute();
            if (response.isSuccessful() && response.body() != null) {
                ImageEntity imageEntity = new ImageEntity();
                imageEntity.setLast(true);
                imageEntity.setUri(UrlUtil.getImageUrl(response.body()));
                imageEntity.setFavorite(false);
                ImageEntity last = localRepository.getLast();
                if (last == null) {
                    localRepository.insertImage(imageEntity);
                } else {
                    last.setUri(UrlUtil.getImageUrl(response.body()));
                    last.setFavorite(false);
                    localRepository.updateImage(last);
                    return Result.success();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return Result.success();
        }
        return Result.failure();
    }
}