package com.example.anothertesttask.adapter;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.anothertesttask.room.ImageEntity;
import com.example.anothertesttask.view.ImageFragment;

import java.util.List;

public class PhotoDetailAdapter extends FragmentPagerAdapter {
    private List<ImageEntity> imageEntityList;

    public PhotoDetailAdapter(FragmentManager fm, List<ImageEntity> imageEntityList) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.imageEntityList = imageEntityList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putString(ImageFragment.ARGS_URI, imageEntityList.get(position).getUri());
        ImageFragment fragment = new ImageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return imageEntityList.size();
    }
}
