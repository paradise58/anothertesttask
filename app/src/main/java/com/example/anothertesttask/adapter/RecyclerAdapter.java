package com.example.anothertesttask.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.anothertesttask.R;
import com.example.anothertesttask.room.ImageEntity;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private ViewHolderListener viewHolderListener;
    private List<ImageEntity> imagesList;

    public interface ViewHolderListener {

        void showFullSizeImage(int imageUri);

    }

    public RecyclerAdapter(ViewHolderListener viewHolderListener) {
        this.viewHolderListener = viewHolderListener;
    }

    public void setData(List<ImageEntity> list) {
        if (imagesList != null) {
            imagesList.clear();
            imagesList.addAll(list);
        } else {
            imagesList = list;
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        view.setOnClickListener(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bindView(imagesList.get(position));
    }

    @Override
    public int getItemCount() {
        return imagesList == null ? 0 : imagesList.size();
    }

    public void removeItem(int position) {
        imagesList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(ImageEntity item, int position) {
        imagesList.add(position, item);
        notifyItemInserted(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        TextView imageName;
        ImageView imageView;
        RelativeLayout viewForeground;
        String uri;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageName = itemView.findViewById(R.id.item_text);
            imageView = itemView.findViewById(R.id.item_image);
            viewForeground = itemView.findViewById(R.id.foreground_view);
        }

        void bindView(ImageEntity image) {
            Glide.with(imageView.getContext()).load(image.getUri()).thumbnail(0.5f).into(imageView);
            String name = "Flickr.com " + image.getId();
            uri = image.getUri();
            this.imageName.setText(name);
        }

        @Override
        public void onClick(View v) {
            viewHolderListener.showFullSizeImage(getAdapterPosition());
        }

    }
}
