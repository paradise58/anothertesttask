package com.example.anothertesttask.view;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;

import com.example.anothertesttask.R;
import com.example.anothertesttask.adapter.PhotoDetailAdapter;
import com.example.anothertesttask.common.App;
import com.example.anothertesttask.room.LocalRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class ImageFragmentContainer extends DialogFragment {
    @Inject
    LocalRepository localRepository;
    private static int position;
    private Disposable disposable;

    public static ImageFragmentContainer getInstance(int pos) {
        position = pos;
        return new ImageFragmentContainer();
    }

    @Override
    public void onStart() {
        super.onStart();
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow()
                    .setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        App.getInstance().getAppComponent().inject(this);
        View view = inflater.inflate(R.layout.fragment_photo_container, container, false);
        ViewPager viewPager = view.findViewById(R.id.view_pager);
        if (getActivity() != null) {
            disposable = localRepository.getFavorite()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(v -> {
                        PhotoDetailAdapter adapter = new PhotoDetailAdapter(getChildFragmentManager(),
                                v);
                        viewPager.setAdapter(adapter);
                        viewPager.setOffscreenPageLimit(3);
                        viewPager.setCurrentItem(position);
                    });

        }
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
        super.onDestroy();
    }
}
