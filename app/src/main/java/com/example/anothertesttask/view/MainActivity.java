package com.example.anothertesttask.view;

import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.example.anothertesttask.R;
import com.example.anothertesttask.common.App;
import com.example.anothertesttask.worker.NotificationWorker;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        Имитация загрузки приложения
        splashScreenDelay();
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolBar();

        if (getSupportFragmentManager().getFragments().size() == 0) {
            goToFirstFragment();
        }
        removeNotification();

        subscribeToNotification();
    }

    private void initToolBar() {
        toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.abc_ic_ab_back_material);
        Objects.requireNonNull(upArrow).setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    void removeNotification() {
        NotificationManager systemService = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Objects.requireNonNull(systemService).cancelAll();
    }

    private void subscribeToNotification() {
        if (isFirstStart()) {
            Constraints constraints = new Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build();
            PeriodicWorkRequest notificationWorkRequest = new PeriodicWorkRequest.Builder(NotificationWorker.class
                    , 15, TimeUnit.MINUTES, 10, TimeUnit.MINUTES)
                    .setConstraints(constraints)
                    .build();

            WorkManager.getInstance().enqueue(notificationWorkRequest);
        }
    }

    private boolean isFirstStart() {
        SharedPreferences sharedPreferences = getSharedPreferences(App.appName, MODE_PRIVATE);
        if (sharedPreferences.getBoolean(App.SHARED_IS_FIRST_START, true)) {
            sharedPreferences.edit().putBoolean(App.SHARED_IS_FIRST_START, false).apply();
            return true;
        }
        return false;
    }

    public void turnActionBar(boolean turn) {
        if (turn) toolbar.setVisibility(View.VISIBLE);
        else toolbar.setVisibility(View.GONE);
    }

    private void goToFirstFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FirstFragment()).commit();
    }

    private void splashScreenDelay() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSharedPreferences(App.appName, MODE_PRIVATE).edit().putBoolean(App.SHARED_IS_APP_LAUNCHED, true).apply();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getSharedPreferences(App.appName, MODE_PRIVATE).edit().putBoolean(App.SHARED_IS_APP_LAUNCHED, false).apply();
    }
}

