package com.example.anothertesttask.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.example.anothertesttask.R;
import com.example.anothertesttask.common.App;
import com.example.anothertesttask.common.GlideApp;
import com.example.anothertesttask.room.ImageEntity;
import com.example.anothertesttask.viewmodels.FirstFragmentViewModel;
import com.example.anothertesttask.viewmodels.ViewModelFactory;

import java.util.Objects;

import javax.inject.Inject;

import static com.example.anothertesttask.view.SecondFragment.SECOND_FRAGMENT_TAG;

public class FirstFragment extends Fragment {

    @Inject
    ViewModelFactory viewModelFactory;
    private FirstFragmentViewModel viewModel;

    public FirstFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getInstance().getAppComponent().inject(this);

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(FirstFragmentViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_first, container, false);

        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);

        TextView errorMessage = view.findViewById(R.id.error_message);

        ImageView imageView = view.findViewById(R.id.simple_drawee_view);
        ImageView likeButton = view.findViewById(R.id.like_button);

        viewModel.getPictureLiveData().observe(this, v -> {
            renderErrorMessage(errorMessage);
            renderLikeButton(likeButton, v);
            GlideApp.with(requireContext())
                    .load(v.getUri())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.5f)
                    .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .fitCenter()
                    .into(imageView);
        });

        imageView.setOnClickListener(v -> goToSecondFragment());

        return view;
    }

    private void goToSecondFragment() {
        Objects.requireNonNull(getActivity()).getSupportFragmentManager()
                .beginTransaction().replace(R.id.fragment_container, new SecondFragment()).addToBackStack(SECOND_FRAGMENT_TAG).commit();
    }

    private void renderLikeButton(ImageView likeButton, ImageEntity image) {
        if (image.isFavorite())
            likeButton.setImageResource(R.drawable.ic_favorite_black_24dp);
        else
            likeButton.setImageResource(R.drawable.ic_favorite_border_black_24dp);

        likeButton.setOnClickListener(v -> {
            if (!image.isFavorite()) {
                viewModel.insert(getNewImageEntity(image));
                image.setFavorite(true);
            } else {
                viewModel.delete(image.getUri());
                image.setFavorite(false);
            }
            viewModel.update(image);
        });
    }

    private ImageEntity getNewImageEntity(ImageEntity imageEntity) {
        ImageEntity newImage = new ImageEntity();
        newImage.setFavorite(true);
        newImage.setUri(imageEntity.getUri());
        newImage.setLast(false);
        return newImage;
    }

    private void renderErrorMessage(TextView errorMessage) {
        if (errorMessage.getVisibility() == View.VISIBLE) {
            errorMessage.setVisibility(View.GONE);
        }
    }


}
