package com.example.anothertesttask.view;


import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.example.anothertesttask.R;
import com.example.anothertesttask.adapter.RecyclerAdapter;
import com.example.anothertesttask.adapter.RecyclerItemTouchHelper;
import com.example.anothertesttask.common.App;
import com.example.anothertesttask.room.ImageEntity;
import com.example.anothertesttask.viewmodels.SecondFragmentViewModel;
import com.example.anothertesttask.viewmodels.ViewModelFactory;
import com.example.anothertesttask.worker.ImageDeleteWorker;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment implements RecyclerAdapter.ViewHolderListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    public static final String SECOND_FRAGMENT_TAG = "secondFragment";

    @Inject
    ViewModelFactory viewModelFactory;
    private RecyclerAdapter adapter;
    private SecondFragmentViewModel viewModel;
    private List<ImageEntity> entityList;
    private View view;

    public SecondFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        App.getInstance().getAppComponent().inject(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SecondFragmentViewModel.class);

        view = inflater.inflate(R.layout.fragment_second, container, false);

        initRecyclerView(view);

        setHasOptionsMenu(true);

        TextView textView = view.findViewById(R.id.not_image_notify);

        viewModel.getFavoritePictureLiveData().observe(this, v -> {
            renderHint(v, textView);
            setAdapterData(v);
        });
        return view;
    }

    private void renderHint(List<ImageEntity> v, TextView textView) {
        if (v.size() == 0) {
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Objects.requireNonNull(getActivity()).onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initRecyclerView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter(this);
        recyclerView.setAdapter(adapter);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallBack = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallBack).attachToRecyclerView(recyclerView);
    }

    @Override
    public void showFullSizeImage(int position) {
        ImageFragmentContainer.getInstance(position).show(getChildFragmentManager(), "PhotoDetail");
    }

    private void setAdapterData(List<ImageEntity> v) {
        entityList = v;
        adapter.setData(v);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainActivity) Objects.requireNonNull(getActivity())).turnActionBar(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) Objects.requireNonNull(getActivity())).turnActionBar(true);
        getActivity().setTitle(requireContext().getResources().getString(R.string.favorite_image_title));
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof RecyclerAdapter.MyViewHolder) {

            final int deletedIndex = viewHolder.getAdapterPosition();
            final ImageEntity deletedItem = entityList.get(deletedIndex);
            adapter.removeItem(viewHolder.getAdapterPosition());

            Constraints constraints = new Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build();
            Data data = new Data.Builder()
                    .putString("uri", deletedItem.getUri())
                    .build();

            OneTimeWorkRequest removeContactRequest = new OneTimeWorkRequest.Builder(ImageDeleteWorker.class)
                    .setInitialDelay(3, TimeUnit.SECONDS)
                    .setConstraints(constraints)
                    .setInputData(data)
                    .build();

            WorkManager.getInstance().enqueue(removeContactRequest);

            Snackbar snackbar = Snackbar
                    .make(view, getResources().getString(R.string.file_deleted), Snackbar.LENGTH_LONG);
            snackbar.setAction(getResources().getString(R.string.cancel), view -> {
                WorkManager.getInstance().cancelWorkById(removeContactRequest.getId());
                adapter.restoreItem(deletedItem, deletedIndex);
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
            WorkManager.getInstance().getWorkInfoByIdLiveData(removeContactRequest.getId()).observe(this, v -> {
                if (WorkInfo.State.SUCCEEDED.equals(v.getState())) {
                    showToast(getResources().getString(R.string.file) + deletedItem.getUri() + getResources().getString(R.string.deleted));
                }
            });
        }

    }

    private void showToast(String message) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
    }
}