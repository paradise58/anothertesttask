package com.example.anothertesttask.dagger.module;


import com.example.anothertesttask.common.FlickrApi;
import com.example.anothertesttask.room.AppDataBase;
import com.example.anothertesttask.room.LocalRepository;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkRepositoryModule {
    public static final String BASE_URL = "https://api.flickr.com/services/";

    @Provides
    @Singleton
    public OkHttpClient getHttpClient() {
        return new OkHttpClient.Builder().addNetworkInterceptor(new StethoInterceptor()).build();
    }

    @Provides
    @Singleton
    public FlickrApi getFlickrApi(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit.create(FlickrApi.class);
    }

    @Provides
    @Singleton
    public LocalRepository getLocalRepository(AppDataBase appDataBase) {
        return new LocalRepository(appDataBase);
    }

}