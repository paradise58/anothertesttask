package com.example.anothertesttask.dagger.module;

import androidx.lifecycle.ViewModel;

import com.example.anothertesttask.dagger.ViewModelKey;
import com.example.anothertesttask.viewmodels.FirstFragmentViewModel;
import com.example.anothertesttask.viewmodels.SecondFragmentViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(FirstFragmentViewModel.class)
    ViewModel firstFragmentViewModel(
            FirstFragmentViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SecondFragmentViewModel.class)
    ViewModel secondFragmentViewModel(
            SecondFragmentViewModel viewModel);

}