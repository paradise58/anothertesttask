package com.example.anothertesttask.dagger.module;

import androidx.room.Room;

import com.example.anothertesttask.R;
import com.example.anothertesttask.common.App;
import com.example.anothertesttask.room.AppDataBase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LocalRepoModule {

    @Provides
    @Singleton
    AppDataBase getAppDataBase() {
        App app = App.getInstance();
        return Room.databaseBuilder(app,
                AppDataBase.class, app.getString(R.string.data_base_name))
                .addMigrations(AppDataBase.MIGRATION_1_2)
                .build();
    }
}
