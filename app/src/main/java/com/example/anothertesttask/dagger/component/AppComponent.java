package com.example.anothertesttask.dagger.component;

import com.example.anothertesttask.dagger.module.LocalRepoModule;
import com.example.anothertesttask.dagger.module.NetworkRepositoryModule;
import com.example.anothertesttask.dagger.module.ViewModelModule;
import com.example.anothertesttask.view.FirstFragment;
import com.example.anothertesttask.view.ImageFragmentContainer;
import com.example.anothertesttask.view.MainActivity;
import com.example.anothertesttask.view.SecondFragment;
import com.example.anothertesttask.worker.ImageDeleteWorker;
import com.example.anothertesttask.worker.ImageWorker;
import com.example.anothertesttask.worker.NotificationWorker;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ViewModelModule.class, NetworkRepositoryModule.class, LocalRepoModule.class})
public interface AppComponent {

    void inject(MainActivity mainActivity);

    void inject(FirstFragment firstFragment);

    void inject(SecondFragment secondFragment);

    void inject(ImageWorker imageWorker);

    void inject(NotificationWorker notificationWorker);

    void inject(ImageFragmentContainer imageFragmentContainer);

    void inject(ImageDeleteWorker imageDeleteWorker);

}
